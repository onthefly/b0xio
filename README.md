# README #

### b0xio the Webinterface for the b0x appliance project ###

* Webinterface for controlling the b0xio SOHO router appliance (own development) 
* Version: 0.1	
* [Website](http://wiki.keksmafia.org) this should soon get some own space here (anybody?) 

### Components ###

* OpenBSD
* rrdtool
* Python 2.7 (will upgrade to 3.x something soon...I think) 
* Flask (with some extensions)
* httpd (with slowCGI extension) (hopefully) 
* virtualenvironment (for devel) 
* custom packages from pip see requirements.txt 

### How do I get set up? ###

* Please refer to the [wiki](https://bitbucket.org/onthefly/b0xio/wiki/Home)
* How to run tests: inside the activated virutalenv cd into www/ and type in "py.test -v"
* This is alpha so nobody should run this in PROD!

### Contribution guidelines ###

* Nothing so far just write me some email or such

### Questions about $whatever? ###

Just write me :) 
* Dirk (dirk at fake.mx)
