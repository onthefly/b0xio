#!flask/bin/python
import os
import unittest


from __init__ import app, db
from database import hosts

class boxioCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        BASE_DIR = os.path.abspath(os.path.dirname(__file__))
        DATABASE_PATH = os.path.join(BASE_DIR, 'test.db')
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + DATABASE_PATH
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()


    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username ,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    def test_make_unique_nickname(self):
        u = User(login='admin', password='test')
        db.session.add(u)
        db.session.commit()
        login = User.make_unique_nickname('admin')
        assert login != 'admin'
        u = User(login=nickname, password='test')
        db.session.add(u)
        db.session.commit()
        nickname2 = User.make_unique_nickname('admin')
        assert nickname2 != 'john'
        assert nickname2 != nickname



if __name__ == '__main__':
    unittest.main()
    
