#!/home/dirk/b0xio/bin/python3.8
# Copyright (c) 2017, Dirk.L
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that 
# the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the 
# following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
# following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or 
# promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# import multiprocessing.dummy
# import multiprocessing
# 
# def ping(ip):
#    success = '/sbin/ping -c1 -q -n'
#    if success:
#        print("{} responded".format(ip))
#    else:
#        print("{} did not respond".format(ip))
#    return success
# 
# def ping_range(start, end):
#     num_threads = 2 * multiprocessing.cpu_count()
#     p = multiprocessing.dummy.Pool(num_threads)
#     p.map(ping, ['192.168.2.%s' % x for x in range(start,end)])
# 
# if __name__ == "__main__":
#     ping_range(0, 255)

import argparse
import os.path,time
from helpers import lilhelpers
from database import hosts,db
from multiprocessing import Process

class rrdcli(lilhelpers):
    def __init__(self):
        lilhelpers.__init__(self)

    def cli(self):
        #parser = optparse.OptionParser(usage="Usage: %prog [options] e.g.: %prog -H", version="%prog 1.0")
        parser = argparse.ArgumentParser()

        parser.add_argument("-C", "--create",
                          dest="create",
                          help="Create a Latency RRD Target",
                          action="store_true",
                      )

        parser.add_argument("-N", "--name",
                          dest="name",
                          help="Target name. Either for plotting or gathering data",
                          action="store"
                      )

        parser.add_argument("-A", "--alias",
                          dest="alias",

                          help="Target alias",
                          action="store"
                      )
        parser.add_argument("-I", "--info",
                          dest="info",
                          help="Target Info.",
                          action="store"
                      )


        parser.add_argument("-P", "--populate",
                          dest="populate",
                          action="store_true",
                          help="Gater data for rrd db. Used for Cronjobs",
                      )

        parser.add_argument("-J", "--justall",
                          dest="justall",
                          action="store_true",
                          help="Gater data for all hosts in db db. Used for Cronjobs",
                      )

        parser.add_argument("-L", "--list",
                          dest="list",
                          help="List all rrd host's stored in app DB",
                          action="store_true"
                      )

        parser.add_argument("-D", "--delete",
                          dest="delete",
                          help="Delete Host from database/monitoring (requires -N)",
                          action="store_true"
                      )

        parser.add_argument("-G", "--graph",
                          dest="graph",
                          help="Create PNG Image of Gatherded data",
                          action="store_true"
                      )

        parser.add_argument("-S", "--system",
                          dest="system",
                          help="Gather/Create System based Data Graphs (e.g heat,firewall,counter)",
                          action="store_true"
                      )

        parser.add_argument("-U", "--graphall",
                          dest="graphall",
                          help="Gather/Create System Data Graphs",
                          action="store_true"
                      )

        args = parser.parse_args()

        if not args.create and not args.populate and not args.list and not args.delete and not \
           args.graph and not args.system and not args.justall and not args.graphall:
            print("[?] Missing required args")
            parser.print_help()
        elif args.create and not args.name:
            print("[?] Sorry missing Hostname")
            parser.print_help()
        elif args.create and args.name and args.alias and args.info:
            self._create_rrd(args.name,args.alias,args.info)
        elif args.graph and not args.name:
            print("[?] Sorry missing Hostname")
        elif args.populate and args.name:
            self._update_rrdb(args.name)
        elif args.list:
            qobj = hosts.query.all()
            print("\n")
            print("+++++++++++++++++++ Hosts +++++++++++++++++++++++")

            for line in qobj:

                print("[+] {}".format(line))

            print("-------------------------------------------------")
        elif args.delete and not args.name:
            print("[!] Missing DB Name!")
        elif args.delete and args.name:
            self._delete_host(args.name)
        elif args.graph and args.name:
            if args.name == "heat":
                self.gen_heat_graph()
            elif args.name == "firewall":
                self._gen_pfrrd()
            elif args.name == "counter":
                self._gen_countrrd()
            else:
                self._create_graph(args.name)
        elif args.system and not args.name:
            print("[!] Missing System metric name to plot eg: rrdcmd.py -G -S -N localhost")
        elif args.system and args.name:

            if args.name == "heat":
                if os.path.exists(self.rrdlib+'/heat_db.rrd'): # hardcoded path..??
                    self._update_heatdb()
                else:
                    self._create_heatdb()

            if args.name == "firewall":
                if os.path.exists(self.rrdlib+'/pf_stats_db.rrd'):
                    self._update_pfrrd()
                else:
                    self._create_pfrrd()

            if args.name == "counter":
                if os.path.exists(self.rrdlib+'/cnt_stats.rrd'):
                    self._update_pfrrd(cnt_only=True)
                else:
                    self._create_cntrrd()

        elif args.justall:
            for host in hosts.query.all():
                ping = Process(target=self._update_rrdb, args=(host,))
                ping.start()
        elif args.graphall:
            for line in hosts.query.all():
                graph = Process(target=self._create_graph, args=(str(line),))
                graph.start()
            return True


if __name__ == '__main__':
    a = rrdcli()
    a.cli()
