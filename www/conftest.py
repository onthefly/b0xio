# conftest.py
import pytest,os,logging
from sqlalchemy import create_engine
from database import hosts
from flask import Flask
from helpers import lilhelpers
from logging.handlers import TimedRotatingFileHandler
import __init__

@pytest.fixture(scope='session')
def connection(request):
    """ Build test sqlite db """
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    DATABASE_PATH = os.path.join(BASE_DIR, 'test.db')
    engine = create_engine('sqlite:///'+DATABASE_PATH)
    hosts.metadata.create_all(engine)
    connection = engine.connect()
    hosts.metadata.bind = engine
    request.addfinalizer(hosts.metadata.drop_all)
    return connection


@pytest.fixture
def db_session(request, connection):
    """ Build testdb scoped session"""
    from sqlalchemy.orm import scoped_session,sessionmaker
    db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=connection))
    request.addfinalizer(db_session.rollback)
    request.addfinalizer(db_session.remove)
    return db_session


@pytest.fixture
def app(request,connection):
    """ Build application context """
    app = __init__.app #Flask(__name__)
    app.config.from_object(__name__) 
    app.testing = True
    app.config['SERVER_NAME'] = 'localhost:5000'
    return app


@pytest.fixture
def logger(request):
    """Build logg facility """
    logger = lilhelpers()
    #logger.setLevel(logger.setLevel.INFO)

    handle = logger.logger
    handle.setLevel(logging.INFO)
    obj = handle.addHandler(handle)

    return obj
