# CopyrIght (c) 2017, Dirk.L
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted
# provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions
# and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of
# conditions and the following disclaimer in the documentation and/or other materials provided
# with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific prior written
# permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
# SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
# OF SUCH DAMAGE.
# ##############################################################################
#
# For heat data
# self._status_out("sysctl hw.sensors.km0|awk '{ print $1 }'|cut -d'=' -f2")
# self._status_out("sysctl hw.sensors.acpitz0.temp0 |awk '{ print $1 }'|cut -d'=' -f2")
# self._status_out("sysctl hw.cpuspeed |awk '{ print $1 }'|cut -d'=' -f2")
# heat_data = self.cmd_out # saves output

import sys
import re
import os
import logging
import subprocess
import datetime
from random import randint
from logging.handlers import TimedRotatingFileHandler # does not do the rotation

try:
    import sqlalchemy
    import validators
except ImportError:
    print('[+] Could not find sqlalchemy and validators module, are they installed?')

######## [Custom libarys] ########
# Fuck python3s weirdness
try:
    from database import hosts,db_session
except ImportError:
    from .database import hosts,db_session


class LilHelpers():
    """Helper Class for creating RRD Graphs """

    def __init__(self):
        if os.uname()[0] == "Linux":
            self.command = "/bin/ping -q -n -c 3 "
            self.rrdtool = "/usr/bin/rrdtool" # test/if installed -conf
            if os.path.isfile(self.rrdtool) is not True:
                print("[?] rrdtool not found. Please install or check path!")
                print("[+] And since Linux sucks, I won't assist ya in installing that damn tool")
                sys.exit(1)
        elif os.uname()[0] == "OpenBSD":
            self.command = "/sbin/ping -q -n -c 3 "
            self.rrdtool = "/usr/local/bin/rrdtool" # test/if installed , -conf
            if os.path.isfile(self.rrdtool) is not True:
                print("[?] rrdtool not found...")
                self._get_rrdtool()
                #sys.exit(1)
        self.base_dir = os.path.abspath(os.path.dirname(__file__))
        self.rrdlib = os.path.join(self.base_dir, 'rrd') # -conf
        self.graphlib = os.path.join(self.base_dir, "static/images") # -conf
        self.cmd_out = None # Used to get the outputs from command calls
        ######## [ Logging] ########
        self.log_level = logging.INFO # Write helper function to trigger external loglevel change
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.log_level)
        self.handler = TimedRotatingFileHandler('/tmp/boxio.log', when='m',\
                                                interval=30, backupCount=5)
        self.handler.setLevel(self.log_level) # why twice?

        self.formatter = \
                         logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)
        ######## [ RRD lib ] ########
        if os.path.isdir(self.rrdlib) is False:
            self._status_out2('mkdir %s' % self.rrdlib)
            self.logger.info( "[!] Created rrd lib directory")


    def hostname(self):
        """Returns name of Host"""
        try:
            self._status_out2('hostname')
            return self.cmd_out
        except AttributeError:
            self.logger.error('[!] command failed. Please investigate or send error report')
            return False



    def reboot(self):
        """Reboots the system """
        try:
            callobj = self._status_out2('reboot')
            return callobj
        except AttributeError:
            self.logger.error('[!] Reboot command failed. Please investigate or send error report')
            return False


    def uptime(self):
        """Get uptime command output"""
        try:
            self._status_out2('uptime')
            return self.cmd_out
        except AttributeError:
            self.logger.error('[!] Reboot command failed. Please investigate or send error report')
            return False


    def _status_out2(self, name):
        with subprocess.Popen(name, stdout=subprocess.PIPE,shell=True) as piipe:
            stdout, stderr = piipe.communicate()
            self.cmd_out = stdout.decode(sys.stdout.encoding)
            if piipe.returncode == int(0):
                self.logger.debug('[!] Command was called successfully: %s', self.cmd_out)
                self.logger.debug('[!+!+!+!] Command had this output: %s', self.cmd_out)
                return True
            self.logger.debug('[!] Command could not be executed: %s ....\
            comand name was %s', str(self.cmd_out), name )
            return False

    def is_valid_host(self,hostname):
        """Check if hostname is a valid ip or Domain"""
        if validators.domain(hostname) is True:
            return True
        elif validators.ipv4(hostname) is True:
            return True
        elif validators.ipv6(hostname) is True:
            return True
        return False


    def _is_host_indb(self,name):
        """Check if hostname is in DB otherwise retun false"""
        try:
            qry = db_session.query(hosts).filter_by(host_name=name).first()
            if str(qry) == str(name):
                return True
            return False
        except sqlalchemy.exc.OperationalError:
            self.logger.error('[!] Failed to run check for: %s ', name)
            return False


    def _get_data(self,q_host): # For single host
        # Only works with hosts inside our sqlite database. Take this in count
        # when writing tests!
        rtt = r"(?P<g1>[0-9]{0,99}\.[0-9]{1,3})\/(?P<g2>[0-9]{0,99}.[0-9]{1,3})\/(?P<g3>[0-9]{0,99}\.[0-9]{1,3})\/(?P<g4>[0-9]{0,99}.[0-9]{1,3})"
        p_l = r"(?P<rtt>[0-9]{1,99}%)"
        try:
            #pout = commands.getoutput( self.command +' %s' % str(q_host)) # Sucks ...
            pout = str(self._status_out2( self.command +'{}'.format(q_host)))
            rttgrp = re.compile(rtt, re.M|re.S)
            rtt_matches = rttgrp.findall(pout)
            plgrp = re.compile(p_l, re.M|re.S)
            pl_matches = plgrp.findall(pout)
            self._status_out2("top -b|grep load|awk '{print $3 ,$4 ,$5}'")
            cpu_load = self.cmd_out
            results = str(pl_matches[0]).strip('%')+':'+str(rtt_matches[0][0])
            self.logger.info('[!] Gathered roundtrip/packetloss data: %s for host %s,\
            load while exec: %s',str(rtt_matches[0][0]),q_host, cpu_load)
            self.logger.debug('[!] Retun values: %s', results)
            return results
        except IndexError as index_error:
            self.logger.error('[!] Index error: %s. Object:%s,%s. Seems like we\
            are offline or host is down',index_error,str(q_host),rtt_matches)
            return False
        except sqlalchemy.exc.OperationalError as op_error:
            self.logger.error('[!] Failed to query db: %s', op_error)
            return False
        except sqlalchemy.exc.InvalidRequestError as req_error:
            self.logger.error('[!] Failed to query db: %s', req_error)
            return False


    def _update_rrdb(self,name):
        rrd_name = name
        rrd_data = self._get_data(rrd_name)
        update_status = self._status_out2(self.rrdtool + " update %s/%s.rrd --template pl:rtt N:%s"\
                                          %(self.rrdlib,rrd_name,rrd_data))
        self.logger.debug('[!] updated rrd return value: %s', str(update_status))
        return update_status


    def _create_heatdb(self):
        rrd_cmd = (" create %s/heat_db.rrd "+
                   "--step 60 "+
                   # Here second DS:Cpufreq:GAUGE:120:0:1500 # (1500*60)
                   " DS:CpuTemp:GAUGE:120:0:1500 "+
                   " DS:Cpufreq:GAUGE:120:0:1500 "+
                   " RRA:MAX:0.5:1:1500"+
                   " RRA:MAX:0.5:1:10500") % self.rrdlib
        self.logger.debug('[!] Created heat_db rrd file in: %s', self.rrdlib)
        return self._status_out2(self.rrdtool+rrd_cmd)


    def _update_heatdb(self):
        name = 'heat_db'
        if os.uname()[0] == "Linux":
            heat_data = randint(100,499)
            cpu_freq = str(randint(-100,1800))
        elif os.uname()[0] == "OpenBSD":
            heat_data = str(randint(100,499))
            self._status_out2(r"sysctl hw.sensors|grep cpu0|cut -d'=' -f2|sed 's/\ degC//'")
            heat_data = self.cmd_out
            self._status_out2("sysctl hw.cpuspeed|cut -d'=' -f2")
            cpu_freq = self.cmd_out
            cumu = heat_data+':'+cpu_freq
            self.logger.info('[!] Sensor data: %s', str(cumu))
            update_rrd = self._status_out2(self.rrdtool+' update {}/{}.rrd --template\
            CpuTemp:Cpufreq N:{}'.format(self.rrdlib,name,cumu))
        return update_rrd


    def gen_heat_graph(self,name='heat_db'):
        """Creates Heat Graph rrd """
        today = str(datetime.datetime.today())
        create_graph = (r" graph %s/heat_db.png -w 785 -h 151 -a PNG "+
                        " --slope-mode --logarithmic --units=si "+
                        " --start -1d --end now --font DEFAULT:7: "+
                        " --title 'sys heat' --watermark '%s'y --vertical-label 'hw.sensor/heat' "+
                        " --right-axis-label 'cpu heat' --right-axis 100:0 "+
                        " --x-grid MINUTE:10:HOUR:1:MINUTE:120:0:%%R --alt-y-grid --rigid "+
                        " DEF:CpuTemp=%s/heat_db.rrd:CpuTemp:AVERAGE "+
                        " DEF:Cpufreq=%s/heat_db.rrd:Cpufreq:AVERAGE "+
                        " AREA:CpuTemp#00D600:'CpuTemp' "+
                        " LINE1:Cpufreq#0066ff:'Cpufreq' "+
                        r" GPRINT:CpuTemp:LAST:'Cur\: %%5.2lf' "+
                        r" GPRINT:CpuTemp:AVERAGE:'Avg\: %%5.2lf' "+
                        r" GPRINT:CpuTemp:MAX:'Max\: %%5.2lf' "+
                        r" GPRINT:CpuTemp:MIN:'Min\: %%5.2lf' ") % \
                        (self.graphlib,today,self.rrdlib,self.rrdlib)
        self.logger.debug('[!] Generating graph with this parameters: %s', create_graph)
        return self._status_out2(self.rrdtool+create_graph)


    def _create_rrd(self,name,alias,info,hours=(24*7)):
        # For latency
        qry = db_session.query(hosts).order_by(hosts.host_id.desc()).first()
        if self._is_host_indb(name) is not True and self.is_valid_host(name) is True:
            try:
                host_id = qry.host_id+1
                host_name = name
                host_info = info
                host_alias = alias
                new_host = hosts(host_id, host_name, host_info, host_alias)
                db_session.add(new_host)
                db_session.commit()
                rrd_create = (" create %s/%s.rrd --step 60 "+
                              " DS:pl:GAUGE:120:0:100 "+
                              " DS:rtt:GAUGE:120:0:10000000 "+
                              " RRA:MAX:0.5:1:%s") % (self.rrdlib, name, self._rra_helper(hours))
                status = self._status_out2(self.rrdtool + rrd_create)
                self.logger.info('[!] Creating Hostentry in DB: %s, %s, %s', host_name,\
                                 host_info, host_alias)
                self.logger.debug('[!] rrd_output: %s %s \nreturned with status:%s',\
                                  self.rrdtool, rrd_create, status)
                #self._crontab_helper()
                return True
            except sqlalchemy.exc.IntegrityError:
                db_session.rollback()
                db_session.remove()
                return False
            except sqlalchemy.orm.exc.UnmappedInstanceError as unmap_err:
                self.logger.error('[!] Instance %s unmapped..  because %s', name,unmap_err)
                db_session.rollback()
                db_session.remove()
                return False
            except sqlalchemy.exc.OperationalError as op_err:
                db_session.rollback()
                db_session.remove()
                self.logger.error('[!] Probably missing values %s,  %s', name,op_err)
                return False
        else:
            self.logger.error('[!] Host %s found twice..returning error', name)
            return False



    def _create_pfrrd(self):
        rrd_cmd =(" create {}/pf_stats_db.rrd ".format(self.rrdlib) +
            "--step 60 " +
            "DS:BytesIn:COUNTER:120:0:10000000000000 " +
            "DS:BytesOut:COUNTER:120:0:10000000000000 " +
            "DS:PktsInPass:COUNTER:120:0:10000000000000 " +
            "DS:PktsInBlock:COUNTER:120:0:10000000000000 " +
            "DS:PktsOutPass:COUNTER:120:0:10000000000000 " +
            "DS:PktsOutBlock:COUNTER:120:0:10000000000000 " +
            "DS:States:GAUGE:120:0:10000000000000 " +
            "DS:StateSearchs:COUNTER:120:0:10000000000000 " +
            "DS:StateInserts:COUNTER:120:0:10000000000000 " +
            "DS:StateRemovals:COUNTER:120:0:10000000000000 " +
            "RRA:MAX:0.5:1:1500 ")
        self.logger.debug("[!] Created pfrrd file in : %s", self.rrdlib)
        return self._status_out2(self.rrdtool+rrd_cmd)

    def _create_cntrrd(self):
        rrd_cmd = (" create {}/cnt_stats.rrd ".format(self.rrdlib) +
                   "--step 60 " +
                   "DS:BytesIn:COUNTER:120:0:10000000000000 " +
                   "DS:BytesOut:COUNTER:120:0:10000000000000 " +
                   "RRA:MAX:0.5:1:1500 ")
        self.logger.debug("[!] Created counter rrd file in : %s",self.rrdlib)
        return self._status_out2(self.rrdtool+rrd_cmd)


    def _update_pfrrd(self,cnt_only=False):
        # Rewrite this
        pre_cmd_fetch = (r"doas pfctl -si |grep -v -e St* -e Cou -e tcp -e cur |awk '{ print $1,$2,$3,$4 }' |\
        sed '/^\ /d ; s/[0-9]\.[0-9]\/s// ; s/[a-z].*[a-z]//; s/\ //'|tr -d ' '|head -n10| awk 1 ORS=':'")
        rrd_cmd = (" update {}/pf_stats_db.rrd --template BytesIn:BytesOut:PktsInPass:PktsInBlock:PktsOutPass:".format(self.rrdlib)+
                   "PktsOutBlock:States:StateSearchs:StateInserts:StateRemovals N:")
        self._status_out2(pre_cmd_fetch)
        pf_out = self.cmd_out
        fpf_out = pf_out.rstrip(':')
        self.logger.debug('[!] Found these matches on pf DATA:\n %s', str(pf_out))
        if cnt_only is True:
            data = str(self._get_thoughput()) # This is where the error is
            rrd_cmd = (" update {}/cnt_stats.rrd --template BytesIn:BytesOut"\
                       .format(self.rrdlib)+" N:") # This file never gets created?
            self.logger.info('[!] updated counter return value: %s',str(data))
            self._status_out2(self.rrdtool+rrd_cmd+data)
        else:
            print(type(fpf_out.split(':')))
            bytes_in, bytes_out, pkgin_passed, pkgin_blocked, pkgout_passed,\
                pkgout_blocked, states, states_searches,\
                states_inserts,states_removals = fpf_out.split(':')
            data = "{}:{}:{}:{}:{}:{}:{}:{}:{}:{}".format(bytes_in,\
                                                          bytes_out,\
                                                          pkgin_passed,\
                                                          pkgin_blocked,\
                                                          pkgout_passed,\
                                                          pkgout_blocked,\
                                                          pkgout_passed,\
                                                          states,\
                                                          states_searches,\
                                                          states_inserts,\
                                                          states_removals)
            self.logger.info('[!] updated pfrrd_db return value: %s',str(data))
            return self._status_out2(self.rrdtool+rrd_cmd+data)


    def _gen_pfrrd(self):
        # This is for pf bandwidth and states graph
        # For the 'Cur\: %5.2lf' related errors (double %% stuff)
        # I can only assume that if you use python3 format strings.
        # That the behaviour regarding % sings changes that they don't
        # have to be escaped anymore?
        create_graph = ( " graph {}/pf_stats_bytes_states.png ".format(self.graphlib) +
            "-w 785 -h 151 -a PNG " +
            "--slope-mode " +
            "--start end-86400 --end now " +
            "--font DEFAULT:7: " +
            "--title 'pf bandwidth and states' " +
            "--watermark '`date`' " +
            "--vertical-label 'bytes/sec' " +
            "--right-axis-label 'states' " +
            "--right-axis 0.001:0 " +
            "--x-grid MINUTE:10:HOUR:1:MINUTE:120:0:%R " +
            "--alt-y-grid --rigid " +
            # here format is missing
            "DEF:BytesIn={}/pf_stats_db.rrd:BytesIn:MAX ".format(self.rrdlib) +
            "DEF:BytesOut={}/pf_stats_db.rrd:BytesOut:MAX ".format(self.rrdlib) +
            "DEF:States={}/pf_stats_db.rrd:States:MAX ".format(self.rrdlib) +
            "CDEF:scaled_States=States,1000,* " +
            "AREA:BytesIn#33CC33:'bytes in ' " +
            r"GPRINT:BytesIn:LAST:'Cur\: %5.2lf' " +
            r"GPRINT:BytesIn:AVERAGE:'Avg\: %5.2lf' " +
            r"GPRINT:BytesIn:MAX:'Max\: %5.2lf' " +
            r"GPRINT:BytesIn:MIN:'Min\: %5.2lf' " +
            "LINE1:scaled_States#FF0000:'states' " +
            r"GPRINT:States:LAST:'Cur\: %5.2lf' " +
            r"GPRINT:States:AVERAGE:'Avg\: %5.2lf' " +
            r"GPRINT:States:MAX:'Max\: %5.2lf' " +
            r"GPRINT:States:MIN:'Min\: %5.2lf' " +
            "LINE1:BytesOut#0000CC:'bytes out' " +
            r"GPRINT:BytesOut:LAST:'Cur\: %5.2lf' " +
            r"GPRINT:BytesOut:AVERAGE:'Avg\: %5.2lf' " +
            r"GPRINT:BytesOut:MAX:'Max\: %5.2lf' " +
            r"GPRINT:BytesOut:MIN:'Min\: %5.2lf' "
        )
        self.logger.debug('[!] Generating graph with this parameters: %s', create_graph)
        return self._status_out2(self.rrdtool+create_graph)

    def _gen_countrrd(self):
        create_graph = ( " graph {}/counter_bytes.png ".format(self.graphlib) +
                         "-w 785 -h 151 -a PNG " +
                         "--slope-mode " +
                         "--start end-86400 --end now " +
                         "--font DEFAULT:7: " +
                         "--title 'pf bandwidth and states' " +
                         "--watermark '`date`' " +
                         "--vertical-label 'bytes/sec' " +
                         "--right-axis-label 'states' " +
                         "--right-axis 0.001:0 " +
                         "--x-grid MINUTE:10:HOUR:1:MINUTE:120:0:%R " +
                         "--alt-y-grid --rigid " +
                         # here format is missing
                         "DEF:BytesIn={}/cnt_stats.rrd:BytesIn:MAX ".format(self.rrdlib)+
                         # here format is missing
                         "DEF:BytesOut={}/cnt_stats.rrd:BytesOut:MAX ".format(self.rrdlib) +
                         "AREA:BytesIn#33CC33:'bytes in' " +
                         "LINE1:BytesOut#0000CC:'bytes out' " +
                         r"GPRINT:BytesIn:LAST:'In\: %5.2lf\t' " +
                         r"GPRINT:BytesOut:LAST:'Out\: %5.2lf' "
        )
        self.logger.debug('[!] Generating graph with this parameters: %s', create_graph)
        return self._status_out2(self.rrdtool+create_graph)



    def _rra_helper(self,hours=25):
        res = (int(hours) * int(60))+60
        return str(res)


    def _delete_host(self,name):
        # Rewrite this since it gives false positives
        db_session.query(hosts).filter_by(host_name=name).delete()
        db_session.commit()
        ret = self._status_out2('rm '+self.rrdlib+'/'+name+'.rrd')
        if ret is True:
            self.logger.debug('[!] Deleted host: %s', name )
        else:
            self.logger.error('[!] Was not able to delete: %s because %s', name,self.cmd_out)
            return False


    def _create_graph(self,name):
        today = str(datetime.datetime.today())
        create_graph = (" graph %s/%s.png -w 785 -h 151 -a PNG --slope-mode "+
                        "--start -1d --end now --font DEFAULT:7: --title '%s' "+ #604800/ -86400
                        "--watermark '%s' --vertical-label Bytes --lower-limit 0 "+
                        " --right-axis 1:0 --x-grid "+
                        # https://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html
                         " MINUTE:10:HOUR:1:MINUTE:120:0:%%R "+
                        #" DAY:7:DAY:1:DAY:1:0:%%R --alt-y-grid "+ # This graphs for a week
                        "--rigid DEF:roundtrip=%s/%s.rrd:rtt:MAX "+
                        "DEF:packetloss=%s/%s.rrd:pl:MAX "+ # hardcoded rrd suffix
                        "CDEF:PLNone=packetloss,0,0,LIMIT,UN,UNKN,INF,IF "+
                        "CDEF:PL10=packetloss,1,10,LIMIT,UN,UNKN,INF,IF "+
                        "CDEF:PL25=packetloss,10,25,LIMIT,UN,UNKN,INF,IF "+
                        "CDEF:PL50=packetloss,25,50,LIMIT,UN,UNKN,INF,IF "+
                        "CDEF:PL100=packetloss,50,100,LIMIT,UN,UNKN,INF,IF "+
                        "LINE1:roundtrip#0000FF:'latency(ms)' "+
                        r"GPRINT:roundtrip:LAST:'Cur\: %%5.2lf' "+
                        r"GPRINT:roundtrip:AVERAGE:'Avg\: %%5.2lf' "+
                        r"GPRINT:roundtrip:MAX:'Max\: %%5.2lf' "+
                        r"GPRINT:roundtrip:MIN:'Min\: %%5.2lf\t' "+
                        r"COMMENT:'pkt loss\:' "+
                        "AREA:PLNone#FFFFFF:'0%%':STACK "+
                        "AREA:PL10#FFFF00:'1-10%%':STACK "+
                        "AREA:PL25#FFCC00:'10-25%%':STACK "+
                        "AREA:PL50#FF8000:'25-50%%':STACK "+
                        "AREA:PL100#FF0000:'50-100%%':STACK ")\
                        % (self.graphlib,name,name,today,self.rrdlib,name,self.rrdlib,name)
        self.logger.debug('[!] Generating graph with this parameters: %s', create_graph)
        return self._status_out2(self.rrdtool+create_graph)


    def _crontab_helper(self):
        # ^\s+Bytes\s(?P<t>[a-zA-Z]+\s+[0-9]+).*(?P<r>)
        # Build crontab file from hosts in database
        with open('boxio.tab', 'w') as fobj:
            fobj.write('HOME='+self.base_dir+'\n')
            fobj.write("* * * * * python rrdcmd.py -J\n")
            fobj.write("*/5 * * * * python rrdcmd.py -U\n\n")
            #fobj.write('HOME='+self.base_dir+'\n')
            fobj.write('* * * * * python rrdcmd.py -S -N heat\n')
            fobj.write('*/5 * * * * python rrdcmd.py -G -N heat\n')
            fobj.write('* * * * * python rrdcmd.py -S -N firewall\n')
            fobj.write('*/5 * * * * python rrdcmd.py -G -N firewall\n')
            fobj.write('* * * * * python rrdcmd.py -S -N counter\n')
            fobj.write('*/5 * * * * python rrdcmd.py -G -N counter\n')
        return self._status_out2('/usr/bin/crontab %s' % str(self.base_dir+'/boxio.tab'))

    def _get_rrdtool(self):
        # Should oneday optionally install rrdtool for the user
        selec = input("[+] .. should I try to install it? (y/n) ")
        if selec == "y":
            print("[+] Installing rrdtool")
            self._status_out2('doas pkg_add rrdtool')
            print(self.cmd_out)
        elif selec == "n":
            print("[+] Skipping rrdtool installation")
        else:
            print("[+] Please enter y(es) or n(o)")


    def _get_thoughput(self):
        b2m = lambda x: x/1024.0
        regex = r'(?P<in>In4\/Pass\:\ )(?P<packets>[0-9]{1,999}) (?P<bytes>[0-9]{1,999})\n.+\n(?P<out>Out4\/Pass:\ )(?P<pout>[0-9]{1,99}) (?P<bout>[0-9]{1,99})'
        self._status_out2("doas pfctl -s Interfaces -vv |grep em0 -A 10 |grep -v -e em0 -e Cleared |awk '{ print $1, $4, $6 }'")
        byte_grp = re.compile(regex, re.M|re.S)
        output = self.cmd_out
        matches = byte_grp.findall(str(output))
        if matches:
            bytes_in = int(b2m(float(matches[0][1])))
            bytes_out = int(b2m(float(matches[0][4])))
            return str(bytes_in)+':'+str(bytes_out)
        return False



if __name__ == '__main__':

    a = LilHelpers()
    print(a)
