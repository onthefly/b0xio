# Copyright (c) 2016, Dirk.L
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that 
# the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the 
# following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
# following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or 
# promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
import hashlib,flask_sqlalchemy,getpass,os
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Integer,create_engine
from flask_login import UserMixin
from sqlalchemy.orm import scoped_session,sessionmaker
from flask import Flask

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DATABASE_PATH = os.path.join(BASE_DIR, 'boxio.db')

app = Flask(__name__)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
sql_uri = app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + DATABASE_PATH


db = SQLAlchemy(app)
engine = create_engine( '%s' % sql_uri, convert_unicode=True )
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

class users(db.Model,UserMixin):
    __tablename__ = 'users'
    uid = db.Column(Integer, primary_key=True)
    login = db.Column(db.String(16), primary_key=True)
    passwd = db.Column(db.String(128) , nullable=False)
    usr_enabled = db.Column(Integer, nullable=False)

    def __init__(self, uid, login, passwd, usr_enabled ):
        self.uid = str(uid).encode('utf-8')
        self.login = login
        self.passwd = passwd
        self.usr_enabled = usr_enabled

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    @property
    def set_password(self, raw_passwd):
        self.passwd = hashlib.sha1(raw_passwd).hexdigest()

    #@property
    def get_id(self):
        return self.uid

    def __repr__(self):
        return str('%r') % self.uid


class hosts(db.Model):
    __tablename__ = 'hosts'
    host_id = db.Column(Integer, primary_key=True)      # DB ID
    host_name = db.Column(db.String(255) )              # FQDN
    host_info = db.Column(db.String(255))               # Additional info
    host_alias = db.Column(db.String(20))               # Alias/shortname for db

    def __init__(self, host_id, host_name, host_info, host_alias): 
        self.host_id = host_id
        self.host_name = host_name
        self.host_info = host_info
        self.host_alias = host_alias


    def __repr__(self):
        return '%s' % self.host_name

    
def init_db(skip_cron=True,skip_password=False):
    # Usage:
    # open terminal:
    # python
    # >>> from www import database
    # >>> from www.database import init_db
    # >>> init_db()

    #from .helpers import lilhelpers
    import helpers
    func = helpers.lilhelpers()

    if skip_cron == False:
        init_cron = helpers.lilhelpers()
        init_cron._status_out2('/usr/bin/crontab %s' % init_cron._crontab_helper())
        print('Successfully installed crontab!')
    else:
        pass
        
    db.create_all()
    if skip_password == False:
        password = getpass.getpass(prompt='[+] Please enter Admin Password: ')
        hashed_pw = hashlib.sha512(password.encode('utf-8')).hexdigest()
        admin = users(int(0),'admin', hashed_pw, int(1))
    elif skip_password == True:
        password = 'admin'
        hashed_pw = hashlib.sha512(password.encode('utf-8')).hexdigest()
        admin = users(int(0),'admin', hashed_pw, int(1))

    localhost = hosts(int(0), 'localhost', "This is a devel test host" , "localhost" )
    # Quick + dirty hack... note to myself: rewrite for better on todo plz
    localhost_rrd = b"rrdtool create rrd/localhost.rrd --step 60 DS:pl:GAUGE:120:0:100 DS:rtt:GAUGE:120:0:10000000 RRA:MAX:0.5:1:1500"
    #commands.getoutput(localhost_rrd)
    func._status_out2(localhost_rrd)

    print('[+] Add Admin to db session...')
    db.session.add(admin)
    db.session.commit()
    print("[+] Commited to Session")
    db.session.add(localhost)
    print("[+] Adding dummy host (localhost for devel only)")
    db.session.commit()
    print("[+] Commited to Session")
    return True



def print_db():
    for line in hosts.query.all():
        print(line.host_id, line.host_name , line.host_info, line.host_alias)


if __name__ == '__main__':
    init_db()
