#!/usr/bin/python
# Copyright (c) 2016, Dirk.L
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that 
# the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the 
# following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
# following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or 
# promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# TODO:  Write a test for testing if we are runnning on an alic plattform??
import pytest,requests,os,hashlib
from helpers import *
from flask import Flask,url_for,current_app
from database import hosts,users
from __init__ import app

a = lilhelpers()

################################### [ DB tests ] ##################

def test_db_lookup(db_session):
    myobj = hosts(len(hosts.query.all())+1, 'homeb0x.local', 'home router', 'homeb0x')
    db_session.add(myobj)
    db_session.flush()
    assert 1 == db_session.query(hosts).count()

def test_db_is_rolled_back(db_session):
    assert 0 == db_session.query(hosts).count()


def test_db_user_lookup(db_session):
    myuser = users(int(0), 'test_user', hashlib.sha512('password').hexdigest(), int(1))
    db_session.add(myuser)
    db_session.flush()

    assert 'b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86' == db_session.query(users).first().passwd
    assert 'test_user' == db_session.query(users).first().login
    assert 1 == db_session.query(users).count()

def test_db_user_is_rolled_back(db_session):
    assert 0 == db_session.query(users).count()

        
################################### [ Tests for _status_out ] ##################


def call_status_out():
    cmd = "true && echo $?"
    return a._status_out(cmd)

def test_call_command():
    assert call_status_out() == True


    
################################### [ Tests for _get_data ] ####################
def call_get_data():
    # The host has to be inside sqlite db
    name = 'localhost'
    return a._get_data(name)
    
def test_call_get_data(db_session):
    assert call_get_data() != False


################################### [ Tests for _create_rrdb ] #################

def call_create_rrd():
    name = 'smtp.gmail.com' # this is going to run into a bug!
    alias = 'gmails smtp?'
    info = "Just the PYTEST entry"
    time = 25
    return a._create_rrd(name,alias,info,time)
    
def test_call_create_rrd(db_session):
    assert call_create_rrd() == True

################################### [ Tests for _update_rrdb ] #################
# These motherfuckers have to be rewritten since they are affected by issue #28 
def call_update_rrdb():
    name = 'localhost'
    return a._update_rrdb(name)

def test_call_update_rrdb():
    assert call_update_rrdb() == True

################################### [ Tests for _delete_rrdb ] #################

def call_delete_rrd():
    name = 'smtp.gmail.com'
    return a._delete_host(name)

    
def test_call_delete_rrd():
    assert call_delete_rrd() == True

    
################################### [ Tests for _update_heatdb ] #################

# This test is very plattform depent (which is actually okay)
def call_update_heatdb():
    return a._update_heatdb()


def test_call_update_heatdb():
    assert call_update_heatdb() == True

################################### [ Tests for _gen_heat_graph ] #################

def call_gen_heat_graph():
    return a.gen_heat_graph()

def test_call_gen_heat_graph():
    assert call_gen_heat_graph() == True

################################### [ Tests for hosts database object ] #################

def call_db_host_first():
    return str(hosts.query.first())

def test_call_db_host_first():
    assert call_db_host_first() == 'localhost'

################################### [ Tests for hosts douplicate check ] #################
def call_douplicate_check():
    alias = 'localhost_127.0.0.1'
    info = "PYTEST DOUBLICATE CHECK entry"
    time = 25
    return a._create_rrd('localhost',alias,info,time)

def test_call_douplicate_check(db_session):
    assert call_douplicate_check() == False

################################### [ Tests for hosts attributes ] #################
def call_host_object():
    myhost = hosts.query.first()
    return myhost.host_name

def test_call_host_object():
    assert call_host_object() == "localhost"

################################### [ Tests for is_valid_host ] #################
def call_is_valid_host():
    hostobj = a.is_valid_host('webmail.fake.mx') 
    return hostobj
    
def test_is_valid_host():
    assert call_is_valid_host() == True

def call_is_valid_ipv4():
    hostobj = a.is_valid_host('212.114.152.1')
    return hostobj

def test_call_is_valid_ipv4():
    assert call_is_valid_ipv4() == True

################################### [ Tests for is_valid_host ] #################

def test_login(app,live_server):
    from flask_login import *
    with app.app_context():
        with current_app.test_client() as c:
            recv = c.get(url_for('login'))
            assert recv.status_code == 200

def test_streamer(app,live_server):
    from flask_login import *
    with app.app_context():
        with current_app.test_client() as c:
            recv = c.get(url_for('stream',lines=15))
            assert recv.status_code != 200
            
def test_index(app):
    with app.app_context() as a:
        with current_app.test_client() as c:
            assert c.get(url_for('index')).status == '302 FOUND'        


#def test_logger(logger):
#    for line in dir(logger):
#        assert line == 1


if __name__ == '__main__':
    pass
