# This is just a short script to get app context in python console
from flask import Flask,url_for,current_app


def app_ctx():
    app = Flask(__name__)
    app.config.from_object(__name__)
    app.testing = True
    app.config['SERVER_NAME'] = 'localhost:5000'
    with app.app_context():
        with current_app.test_client() as cli:
            return cli
