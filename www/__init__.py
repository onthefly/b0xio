#!/usr/bin/python
# Copyright (c) 2017, Dirk.L
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that 
# the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the 
# following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
# following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or 
# promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Notes:
# Flask-login: https://flask-login.readthedocs.io/en/latest/
#
# Steam method from:
# http://stackoverflow.com/questions/35540885/display-the-contents-of-a-log-file-as-it-is-updated
#
#
# APP Framework imports
#
#from flask import #Flask,render_template,flash,request,g,redirect
from flask import *
#from .forms import AddHostForm,SyslogForm,InitForm
from flask_login import LoginManager, login_user, logout_user, \
    login_required, current_user, UserMixin, \
    confirm_login, fresh_login_required, current_user
from .helpers import *
#import flask_pagination
from flask import _app_ctx_stack
from flask_paginate import Pagination,get_page_parameter
# Model imports
#
from .database import *
from .safe_url import *
import hashlib,os,datetime
import wtforms,re,flask
from sqlalchemy.orm import scoped_session,sessionmaker # for py.test
from .database import users,hosts,db_session
from time import sleep
from .helpers import lilhelpers

#
# APP settings
#
app = Flask(__name__)
app.secret_key = '\x02L\x83\xdb\xfe\x0co\asewa9870q98c0s/\xf3mhS`\x03\x9f\x93FG\xc4$\xc3\x98h\x0cJ;Ed\xf7{'
#app.config["REMEMBER_COOKIE_DURATION"] = datetime.timedelta(minutes=14 )
app.config.from_object(__name__)
app.config['REMEMBER_COOKIE_DURATION'] = datetime.timedelta(minutes=30)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "auth"
login_manager.setup_app(app)


def setlist(lst=[]):
       return list(set(lst))

@app.route('/', methods=['GET','POST'])
def index():
    logout_user()
    return redirect(url_for('login'))


@app.route('/auth',methods=['POST', 'GET'] )
def auth():
    from .forms import LoginForm
    if request.method == 'POST' and 'username' in request.form:
        login = request.form['username']
        password = request.form['password']
        result = users.query.filter_by(login="admin").first()
        admin = users(result.uid, result.login, result.passwd, result.usr_enabled)

        if login == result.login and hashlib.sha512(password.encode('utf-8')).hexdigest() == result.passwd:
            remember = request.form.get("remember", "no") == 'yes'
            flash('You have been logged in')
            session['logged_in'] = True
            #session['remember'] = login.remember
            next = flask.request.args.get('next')
            g.user = current_user

            login_user(admin,remember)
            #print( request.url_root.rstrip('/')+flask.request.path)
            if not is_safe_url(request.url_root+flask.request.path):
                return abort(400)
            session.permanent = True
            return redirect( flask.request.path or url_for('dashboard'))
        else:
            flash('Sorry some Kind of Error occoured. Check your password?')
            print(login, result.login,hashlib.sha512(password.encode('utf-8')).hexdigest(),result.passwd)
            return redirect(url_for('login'))
    return redirect(url_for('dashboard'))


@app.route('/login')
def login(methods=['POST','GET']):
    # Serves Loginform
    # LoginForm calls /auth 
    from .forms import LoginForm
    form = LoginForm()
    session.pop('logged_in', None)
    return render_template('login.html', form=form)



@app.route('/index', methods=['POST', 'GET'])
#@login_required
def dashboard():
    # To catch: KeyError
    from .database import hosts
    from .forms import DashboardForm

    pages_choice = [4, 8, 16, 24, 32, 48]
    count = int(5)
    hostobj = hosts.query.all()
    AVAILABLE_CHOICES = setlist(hostobj)
    items_list = []
    strip_chars = ['(', '\'', ',', ')', '\--']
    form = DashboardForm()

    #for line in dir(hosts.query): print line

    try:
        PER_PAGE = pages_choice[int(request.args.get('entrys_per_page'))] 
    except TypeError:
        PER_PAGE = pages_choice[0]

    q = request.args.get('q')
    if q:
        search = True

    try:
        page = int(request.args.get('page', 1))
    except ValueError:
        page = 1

    items_list = db_session.query(hosts.host_name, hosts.host_info, hosts.host_id).limit(4).all()

    pagination = Pagination(page=page, total=len(AVAILABLE_CHOICES), search=False, record_name='hosts')
    my_start_range = (PER_PAGE * page) - (PER_PAGE - 1 ) -1

    return render_template('dasboard.html', items=hostobj, pagination=pagination ,form=form, items_list=items_list)



@app.route('/syslog', methods=['POST', 'GET'])
@login_required
def syslog():
    from .forms import SyslogForm
    syslog_form = SyslogForm()
    lines = None
    a = lilhelpers()
    #for line in dir(a): print line
    print(request.args.get('log_level'))
    if request.method == "GET" and lines == None:
        lines = request.args.get('lines') 
        #request.args.get('log_level')

        if syslog_form.lines.data == None:
            syslog_form.lines.data = lines
            return render_template('syslog.html', syslog_form=syslog_form,lines=syslog_form.lines.data)

    if request.args.get('lines') != None:
        syslog_form.lines.data = lines
        return render_template('syslog.html', syslog_form=syslog_form,lines=lines)

    return render_template('syslog.html', syslog_form=syslog_form,lines=15) # Default view+args


@app.route('/stream')
@app.route('/stream/<lines>/')
@login_required
def stream(lines):
    def generate(lines=lines):
        fobj = open('/tmp/boxio.log', 'r')
        sq = fobj.readlines()
        for num, line in enumerate(reversed(sq)):
            yield line
            try:
                if int(num) == int(lines):
                    break
            except ValueError as e:
                break
        fobj.close()
    if request.args.get('lines') != None:
        lines = request.args.get('lines')
    return app.response_class(generate(lines), mimetype='text/plain')


@app.route('/reboot', methods=['POST','GET'])
@login_required
def reboot():
    a.reboot()
    flash('Rebooting System')
    return redirect(url_for(('settings')))


@app.route('/halt', methods=['POST','GET'])
@login_required
def halt():
    a.halt()
    flash('Stopping the System')
    return redirect(url_for(('settings')))


@app.route('/settings', methods=['POST', 'GET'])
@login_required
def settings():
    from forms import SettingsForm
    a = lilhelpers()
    form = SettingsForm(request.form)
    if request.method == "POST":
        return render_template('settings.html', form=form)
        request.script_root = url_for('index', _external=True)
    return render_template('settings.html', form=form)


@app.route('/uptime', methods=['POST','GET'])
@login_required
def uptime():
    # extend helpers to be privateprivate and that
    #a object instance of the class has to be an argument
    a = lilhelpers()
    return a.uptime()


@app.route('/network',methods=['POST', 'GET'])
@login_required
def network():
    from forms import SettingsForm,AddHostForm,SyslogForm,InitForm,DHCPForm
    from wtforms import SelectField,SubmitField

    a = lilhelpers() # Hostname helper
    host_entrys = hosts.query.all()

    mydyn = type('SelectHost', (wtforms.Form,), {})
    mydyn.hostSelect = SelectField('Current hosts in DB', choices=[x for x in enumerate(host_entrys)], default=0)
    mydyn.submitbtn = SubmitField('Remove Host')
    meta_form = mydyn(request.form)
    form = SettingsForm(request.form)
    dhcp_form = DHCPForm(request.form)
    form.hostname.data = str(a.hostname())
    addhost_form = AddHostForm(request.form)
    if request.method == "GET":
        host_name = request.args.get('hostname')
        host_alias = request.args.get('host_alias')
        host_info = request.args.get('host_info')
        if host_name != None and host_alias != None and host_info != None:
            if a._create_rrd(host_name, host_alias, host_info, int(24*7) ) == True:
                flash('Host %s was created successfull. Keep in mind graphs take ~5 Min.' % host_name)
                return redirect(url_for('network'))
    elif request.method == "POST": #and meta_form.validate():
        host_obj = int(request.form.get('hostSelect') ) 
        choices=[x for x in enumerate(host_entrys)]
        host = str(choices[host_obj][1]) # this could bug around
        result = str(hosts.query.filter_by(host_name=host).first())
        if result == host:
            if a._delete_host(host) == True:
                flash('Host %s deleted successfull' % host)

    return render_template('network.html', form=form,host_entrys=host_entrys,addhost_form=addhost_form, meta_form=meta_form, dhcp=dhcp_form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('login'))


#@login_manager.unauthorized_handler
def unauthorized():
    flash('Sorry you are not authorized')
    return redirect(url_for('login'))


@app.route('/init',methods=['GET', 'POST'] )
@login_required
def init():
    init = InitForm(request.form)
    return render_template('init.html',init=init)


@login_manager.user_loader
def load_user(uid):
    try:
        return users.query.filter_by(uid=uid).first()
    except:
        return None

# Error handlers
@app.errorhandler(404)
def in_error(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500
