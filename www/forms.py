# Copyright (c) 2016, Dirk.L
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that 
# the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the 
# following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
# following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or 
# promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR 
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
from flask_wtf import *
from wtforms import * 
from wtforms.validators import *
from .safe_url import *

class LoginForm(Form):
    username = TextField('Username', validators=[DataRequired(),Length(max=12)])
    password = PasswordField('Password', validators=[DataRequired(),Length(min=8)])
    remember = BooleanField('remember', default = False, validators=[DataRequired()])
    login = SubmitField('Login')

    
class SettingsForm(Form):
    hostname = StringField('Hostname', validators=[DataRequired()])
    passwd = PasswordField('Password', validators=[DataRequired(), EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('retype Password', validators=[DataRequired()])
    localdomain = StringField('Local search domain', validators=[DataRequired()], default='my.domain')
    routerip = StringField('Router IP (Internal)', validators=[DataRequired()], default='192.168.2.1')
    lanip_range = StringField('Enter LAN IP Range', validators=[DataRequired()],default='192.168.2.5-101')
    run_dsl = SubmitField('Run DSL Setup')
    change_dsl = SubmitField('Change Internet Settings')

    
class AddHostForm(Form):
    hostname = StringField('Hostname (FQDN)', [DataRequired(),Length(max=64)])
    host_alias = StringField('Host alias', [DataRequired(),Length(max=20)])
    host_info = TextAreaField('Info', [DataRequired(),Length(max=200)])
    addhost = SubmitField('Add Host')

    
class SyslogForm(Form):
    lines = IntegerField('Lines to show', [DataRequired(),Length(min=1)],default=15)
    log_level = SelectField('Log Level', choices=[('INFO','INFO'),('ERROR','ERROR'), ('DEBUG','DEBUG')],default=0)
    download = SubmitField('Download Log Archives')
    turncate = SubmitField('Delete all Logs')
    submit = SubmitField('submit')

    
class InitForm(Form):
    init = SubmitField('Reinitalize Networks')
    submit = SubmitField('submit')


class DHCPForm(Form):
    # This is just the simple form for the beginning later we make a more complex variant with
    # host definitions etc
    subnet = StringField('Subnet', validators=[IPAddress()], default='192.168.2.0')
    netmask = StringField('Netmask', validators=[IPAddress()], default='255.255.255.0')
    o_dns = StringField('DNS Servers', validators=[IPAddress()],default='127.0.0.1,8.8.8.8')
    o_routers = StringField('Routers', validators=[IPAddress()], default="192.168.2.1")
    o_iprange = StringField('IP Range', validators=[IPAddress()], default="192.168.2.5-99")
    submit = SubmitField('submit')


class DashboardForm(Form):
    entrys_per_page = SelectField('Eintraege',choices=[('0', '4'),('1', '8'),('2', '16'),('3', '32'),\
                                                       ('4', '40'), ('5', '48')], default='0')
